from deap import tools, base
from multiprocessing import Pool
from ga_scheme import eaMuPlusLambda
# from deap.algorithms import eaMuPlusLambda
from numpy import random as rnd
import numpy as np
from deap import creator
from deap import benchmarks
import random

creator.create("BaseFitness", base.Fitness, weights=(1.0,))
creator.create("Individual", np.ndarray, fitness=creator.BaseFitness)

# def mutation(individual):
#     n = len(individual)
#     for i in range(n):
#         if rnd.random() < n * 0.15:
#             individual[i] += rnd.normal(0.0, 0.2)
#             individual[i] = np.clip(individual[i], -5, 5)
#     return individual,

def mutation(individual, max_size = 2):
    n = len(individual)
    samples = random.sample(range(n), max_size)
    for i in samples:
        if rnd.random() < max_size * 0.15:
            individual[i] += rnd.normal(0.0, 0.6)
            individual[i] = np.clip(individual[i], -5, 5)
    return individual,

def crossover(individual1, individual2):
    for i, (gen1, gen2) in enumerate(zip(individual1, individual2)):
        alf = np.random.choice([i/4 for i in range(5)])
        individual1[i] = (1 - alf) * gen1 + alf * gen2
        individual2[i] = alf * gen1 + (1 - alf) * gen2
    return individual1, individual2


class SimpleGAExperiment:
    def factory(self):
        return rnd.random(self.dimension) * 10 - 5

    def __init__(self, function, dimension, pop_size, iterations):
        self.pop_size = pop_size
        self.iterations = iterations
        self.mut_prob = 0.6
        self.cross_prob = 0.3

        self.function = function
        self.dimension = dimension

        # self.pool = Pool(5)
        self.engine = base.Toolbox()
        # self.engine.register("map", self.pool.map)
        self.engine.register("map", map)
        self.engine.register("individual", tools.initIterate, creator.Individual, self.factory)
        self.engine.register("population", tools.initRepeat, list, self.engine.individual, self.pop_size)
        # self.engine.register("mate", tools.cxOnePoint)
        self.engine.register("mate", crossover) 
        # self.engine.register("mutate", tools.mutGaussian, mu=0, sigma=0.5, indpb=0.2)
        self.engine.register("mutate", mutation)
        self.engine.register("select", tools.selTournament, tournsize=4)
        # self.engine.register("select", tools.selRoulette)
        self.engine.register("evaluate", self.function)

    def run(self):
        pop = self.engine.population()
        hof = tools.HallOfFame(3, np.array_equal)
        stats = tools.Statistics(lambda ind: ind.fitness.values[0])
        stats.register("avg", np.mean)
        stats.register("std", np.std)
        stats.register("min", np.min)
        stats.register("max", np.max)

        pop, log = eaMuPlusLambda(pop, self.engine, mu=self.pop_size, lambda_=int(self.pop_size*0.8), cxpb=self.cross_prob, mutpb=self.mut_prob,
                                  ngen=self.iterations,
                                  stats=stats, halloffame=hof, verbose=True)
        print("Best = {}".format(hof[0]))
        print("Best fit = {}".format(hof[0].fitness.values[0]))
        return log

from functions import rastrigin
if __name__ == "__main__":

    def function(x):
        res = rastrigin(x)
        return res,

    dimension = 100
    pop_size = 100
    iterations = 1000
    scenario = SimpleGAExperiment(function, dimension, pop_size, iterations)
    log = scenario.run()
    from draw_log import draw_log
    draw_log(log)
